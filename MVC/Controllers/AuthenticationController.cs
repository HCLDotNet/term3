﻿using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MVC.Controllers
{
    public class AuthenticationController : Controller
    {
        EmployeeMgmtEntities1 db = new EmployeeMgmtEntities1();
        // GET: Authentication
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(true)]
        public ActionResult Login(User user)
        {
            var query = from u in db.Users
                        where u.username == user.username && u.password == user.password
                        select u;
            User matchedUser = query.FirstOrDefault();
            if (matchedUser != null)
            {
                //In order to setup login, add <authentication mode="Forms"> to web.config
                FormsAuthentication.SetAuthCookie(matchedUser.username, true);
                return RedirectToAction("Index","Employee");
            } else
            {
                ViewData["failed"] = "Authentication failed";
                return View();
            }

        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Employee");
        }
    }
}