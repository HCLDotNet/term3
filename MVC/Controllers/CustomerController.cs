﻿using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Demo
        public ActionResult Index()
        {
            Customer c = new Customer();
            c.Name = "Camnh";
            c.Code = "CAMNH-123";
            c.Amount = 2090.34;
            return View("DisplayCustomer", c);
        }

        public ActionResult InputCustomer()
        {
            return View("InputCustomer");
        }

        public ActionResult ReceiveInputCustomer()
        {
            Customer c = new Customer();
            c.Name = Request.Form["Name"];
            c.Code = Request.Form["Code"];
            c.Amount = Convert.ToDouble(Request.Form["Amount"]);
            return View("DisplayCustomer", c);
        }

        public ActionResult PassViewData()
        {
            ViewData["myData"] = "123";
            return View("PassViewData");
        }

        /**
         * Display dynamic data to view by extending model.
         */
        public ActionResult DisplayDataDynamic()
        {
            CustomerDiscount customerDiscount = new CustomerDiscount();
            customerDiscount.Name = Request.Form["Name"];
            customerDiscount.Code = Request.Form["Code"];
            customerDiscount.Amount = Convert.ToDouble(Request.Form["Amount"]);
            customerDiscount.CalculateDiscount();
            
            return View(customerDiscount);
        }

        
    }
}