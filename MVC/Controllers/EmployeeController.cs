﻿using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    //Add authorize annotation to prevent unauthenticated access
    //Type is controller level filter ==> filter for All controller methods
    [Authorize]
    public class EmployeeController : Controller
    {
        EmployeeMgmtEntities db = new EmployeeMgmtEntities();
        // GET: Employee

        //Add authorize annotation to prevent unauthenticated access
        //Type is action filter ==> filter for Only Index() method
        //[Authorize]
        public ActionResult Index()
        {
            IEnumerable<Employee> employees = db.Employees;
            return View(employees);
        }

        /**
         * View Detail of employee
         */
        public ActionResult Show(int id)
        {
            Employee employee = db.Employees.Find(id);
            return View(employee);
        }

        public ActionResult Create()
        {
            Employee employee = new Employee();
            return View(employee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Employee employee)
        {
            db.Employees.Add(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            Employee employee = db.Employees.Find(id);
            return View(employee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Employee employee)
        {
            db.Entry(employee).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            Employee employee = db.Employees.Find(id);
            db.Employees.Remove(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}