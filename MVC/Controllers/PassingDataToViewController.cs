﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class PassingDataToViewController : Controller
    {
        // GET: PassingDataToView
        public ActionResult Index()
        {
            ViewData["title"] = "View data title";
            ViewBag.Title = "View bag title";
            TempData["title"] = "Temp data title";
            Session["title"] = "Session title";
            return View();
        }
    }
}