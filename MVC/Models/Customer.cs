﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class Customer
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public double Amount { get; set; }
    }
}