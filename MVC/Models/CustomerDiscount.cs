﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class CustomerDiscount : Customer
    {
        public int Discount { get; set; }

        public void CalculateDiscount()
        {
            if (Amount > 25000)
            {
                Discount = 10;
            }
            else
            {
                Discount = 5;
            }
        }
    }
}