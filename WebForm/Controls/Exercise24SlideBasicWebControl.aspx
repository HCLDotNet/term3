﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Exercise24SlideBasicWebControl.aspx.cs" Inherits="Controls.Exercise24SlideBasicWebControl" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Select files starting with
            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
            </asp:DropDownList>
            <br />
            <br />
            <asp:Label ID="Label1" runat="server" Visible="False"></asp:Label>
            <br />
            <br />
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Exercise25SlideBasicWebControl.aspx">Go to upload file page</asp:HyperLink>
        </div>
    </form>
</body>
</html>
