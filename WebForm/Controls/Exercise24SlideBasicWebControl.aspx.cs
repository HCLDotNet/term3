﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Controls
{
    public partial class Exercise24SlideBasicWebControl : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = "";
            if (!DropDownList1.Items.Contains(new ListItem("a","a")))
            {
                DropDownList1.Items.Add(new ListItem("a", "a"));
            }
            if (!DropDownList1.Items.Contains(new ListItem("b", "b")))
            {
                DropDownList1.Items.Add(new ListItem("b", "b"));
            }
            if (!DropDownList1.Items.Contains(new ListItem("c", "c")))
            {
                DropDownList1.Items.Add(new ListItem("c", "c"));
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {            
            DirectoryInfo directoryInfo = new DirectoryInfo(Server.MapPath("~/Uploads"));
            FileInfo[] fileInfos = directoryInfo.GetFiles();
            Label1.Visible = true;
            foreach (FileInfo fileInfo in fileInfos)
            {
                if (fileInfo.Name.StartsWith(DropDownList1.SelectedValue))
                {
                    Label1.Text += fileInfo.Name+"<br/>";
                }
            }
        }
    }
}