﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Exercise25SlideBasicWebControl.aspx.cs" Inherits="Controls.Exercise25SlideBasicWebControl" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:FileUpload ID="FileUpload1" runat="server" />
            <br />
        </div>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Upload" />
        <br />
        <br />
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Exercise24SlideBasicWebControl.aspx">Go to Exercise24</asp:HyperLink>
        <br />
        <br />
        <asp:Label ID="Label1" runat="server" Visible="False"></asp:Label>
    </form>
</body>
</html>
