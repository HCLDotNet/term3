﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Exercise33SlideBasicWebControl.aspx.cs" Inherits="Controls.Exercise33SlideBasicWebControl" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Menu ID="Menu1" runat="server" OnMenuItemClick="Menu1_MenuItemClick">
            <Items>
                <asp:MenuItem Text="View 1" Value="View 1"></asp:MenuItem>
                <asp:MenuItem Text="View 2" Value="View 2"></asp:MenuItem>
            </Items>

        </asp:Menu>
        <div>
            <asp:MultiView ID="MultiView1" runat="server">
                <asp:View ID="View1" runat="server">
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/download.jpg"/>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/sample-là-gì.jpg" />
                </asp:View>
            </asp:MultiView>
        </div>
    </form>
</body>
</html>
