﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Controls
{
    public partial class Exercise33SlideBasicWebControl : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
        {
            string selectedValue = Menu1.SelectedValue;
            if (selectedValue == "View 1")
            {
                MultiView1.SetActiveView(View1);
            }

            if (selectedValue == "View 2")
            {
                MultiView1.SetActiveView(View2);
            }
        }
    }
}