﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Controls
{
    public partial class Exercise36SlideBasicWebControl : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {            
            DataSet ds = new DataSet();
            ds.ReadXml(Server.MapPath("~/cd_catalog.xml"));

            DataTable dataTable = ds.Tables[0];
            TableRow headerRow = new TableRow();
            TableCell titleHeaderCell = new TableCell();
            titleHeaderCell.Text = "Title";
            titleHeaderCell.Style.Add("color", "red");

            TableCell artistHeaderCell = new TableCell();
            headerRow.Cells.AddRange(new TableCell[] { titleHeaderCell, artistHeaderCell });
            artistHeaderCell.Text = "Artist";
            artistHeaderCell.Style.Add("color", "red");

            Table1.Rows.Add(headerRow);          

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                TableRow tableRow = new TableRow();

                string title = dataTable.Rows[i]["Title"].ToString();                
                TableCell titleCell = new TableCell();
                titleCell.Text = title;

                string artist = dataTable.Rows[i]["ARTIST"].ToString();
                TableCell artistCell = new TableCell();
                artistCell.Text = artist;

                tableRow.Cells.AddRange(new TableCell[] { titleCell, artistCell });

                Table1.Rows.Add(tableRow);
            }
        }
    }
}