﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Controls
{
    public partial class ListBox : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ListBox1.Items.Add("Java");
            ListBox1.Items.Add(".NET");
            ListBox1.Items.Add("Oracle");
            ListBox1.Items.Add("Testing");
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox1.Items.Clear();
            if (DropDownList1.SelectedIndex == 0)
            {
                ListBox1.Items.Add("Java");
                ListBox1.Items.Add(".NET");
                ListBox1.Items.Add("Oracle");
                ListBox1.Items.Add("Testing");
            }
            if (DropDownList1.SelectedIndex == 1)
            {
                ListBox1.Items.Add("C");
                ListBox1.Items.Add("C++");
                ListBox1.Items.Add("Oracle");
                ListBox1.Items.Add("ASP.NET");
            }
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //output
            Label1.Visible = true;
            //hobbies
            string output = string.Format("Your choice is Name: {0}<br/> Country: {1} <br/> Course: {2} <br/> Date: {3} Hobbies: {4}",
                TextBox1.Text, DropDownList1.Items[DropDownList1.SelectedIndex].Text, ListBox1.Items[ListBox1.SelectedIndex].Text, 
                Calendar1.SelectedDate.ToShortDateString(), CheckBoxList1.Items[CheckBoxList1.SelectedIndex].Text);
            Label1.Text = output;
        }
    }
}