﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MultiViewSite.aspx.cs" Inherits="Controls.MultiViewSite" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form runat="server">
        <asp:Button ID="Button1" runat="server" Text="Go to view 1" OnClick="Button1_Click" />
        <asp:Button ID="Button2" runat="server" Text="Go to view 2" OnClick="Button2_Click" />

        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="View1" runat="server">
                <div>
                    Name
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                    <br />
                    <br />
                    Password
                    <asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
                </div>
            </asp:View>
            <asp:View ID="View2" runat="server">
                Hello from view 2
            </asp:View>
        </asp:MultiView>
    </form>

 

</body>
</html>
