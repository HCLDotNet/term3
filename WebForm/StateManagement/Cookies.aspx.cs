﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StateManagement
{
    public partial class Cookie : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if page refreshes
            if (!Page.IsPostBack)
            {
                //Get cookie values.
                TextBox1.Text = Response.Cookies["login"].Value;
                TextBox2.Text = Response.Cookies["name"].Value;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Cookies["login"].Value = TextBox1.Text;
            Response.Cookies["name"].Value = TextBox2.Text;
        }
    }
}