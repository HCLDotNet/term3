﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Exercise17SlideStateManagement2.aspx.cs" Inherits="StateManagement.Exercise17SlideStateManagement2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server" method="get">
        <div>
            Adjective to describe
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click"  />
        </div>
        <asp:HiddenField ID="HiddenField1" runat="server" OnValueChanged="HiddenField1_ValueChanged" />
    </form>
</body>
</html>
