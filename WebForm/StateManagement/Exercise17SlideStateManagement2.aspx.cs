﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StateManagement
{
    public partial class Exercise17SlideStateManagement2 : System.Web.UI.Page
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
            //if request comes from Exercise17SlideStateManagement is not null, then store in hidden form
            if (Request.Form["TextBox1"] != null)
            {
                HiddenField1.Value = Request.Form["TextBox1"];
            }
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Exercise17SlideStateManagement3.aspx?name=" +HiddenField1.Value  + "&adjective=" + TextBox1.Text);
        }

        protected void Hidden1_ValueChanged(object sender, EventArgs e)
        {

        }

        protected void HiddenField1_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}