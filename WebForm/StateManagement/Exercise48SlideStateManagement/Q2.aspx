﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Q2.aspx.cs" Inherits="StateManagement.Exercise48SlideStateManagement.Q2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Choose B<br />
            <br />
            <asp:RadioButtonList ID="RadioButtonList1" runat="server" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                <asp:ListItem>A</asp:ListItem>
                <asp:ListItem>B</asp:ListItem>
            </asp:RadioButtonList>
            <br />
            <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Prev" />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Next" />
            <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" style="height: 26px" Text="Finish" />
        </div>
    </form>
</body>
</html>
