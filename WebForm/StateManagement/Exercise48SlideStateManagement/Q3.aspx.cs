﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StateManagement.Exercise48SlideStateManagement
{
    public partial class Q3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Q3"] != null)
                {
                    RadioButtonList1.SelectedValue = Session["Q2"].ToString();
                }
            }    
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("Result.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Q2.aspx");
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["Q3"] = RadioButtonList1.SelectedItem == null ? "" : RadioButtonList1.SelectedItem.Value;
        }
    }
}