﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StateManagement.Exercise48SlideStateManagement
{
    public partial class Result : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string[] answers = new string[] { "A", "B", "C" };
            int score = 0;
            if (Session["Q1"].ToString() == "A")
            {
                score++;
            }

            if (Session["Q2"].ToString() == "B")
            {
                score++;
            }

            if (Session["Q3"].ToString() == "C")
            {
                score++;
            }

            Label1.Text = ""+score;
        }
    }
}