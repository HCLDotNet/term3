﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StateManagement
{
    public partial class Cart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {
                if (Session["cart"] != null)
                {
                    ArrayList ar = (ArrayList)Session["cart"];
                    CheckBoxList1.Items.Clear();
                    for (int i = 0; i < ar.Count; i++)
                    {
                        CheckBoxList1.Items.Add(new ListItem(ar[i].ToString(), ar[i].ToString()));
                    }
                }
            }
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ArrayList ar = (ArrayList)Session["cart"];

            foreach (ListItem listItem in CheckBoxList1.Items)
            {
                if (listItem.Selected)
                {
                    ar.Remove(listItem.Value);
                }
            }
            //Reset session
            CheckBoxList1.Items.Clear();
            for (int i = 0; i < ar.Count; i++)
            {
                CheckBoxList1.Items.Add(new ListItem(ar[i].ToString(), ar[i].ToString()));
            }
        }
        
    }
}