﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StateManagement
{
    public partial class Session : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Link to Cart.aspx
        }

        protected void CheckBoxList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ArrayList ar = new ArrayList();

            foreach (ListItem listItem in CheckBoxList1.Items)
            {
                if(listItem.Selected)
                {
                    ar.Add(listItem.Value);
                }
            }
            Session.Add("cart", ar);

            Response.Redirect("Cart.aspx");
        }
    }
}