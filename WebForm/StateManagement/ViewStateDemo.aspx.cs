﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StateManagement
{
    public partial class ViewStateDemo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //store data in view state without using session
            //If using viewstate, when view page source, there are hidden fields to indicate view state
            ViewState["username"] = TextBox1.Text;
            ViewState["email"] = TextBox2.Text;
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            //Restore data in view state, used for persisting field in validation form
            TextBox1.Text = ViewState["username"].ToString();
            TextBox2.Text = ViewState["email"].ToString();
        }
    }
}