﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Exercise29SlideAspNetIntro.aspx.cs" Inherits="WebForm.Exercise29SlideAspNetIntro" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Login&nbsp;
            <input id="Text1" type="text" runat="server" /><br />
            <br />
            Password
            <input id="Password1" type="password" runat="server"/><br />
            <br />
            Retype
            <input id="Password2" type="password" runat="server" /><br />
            <br />
            <input id="Submit1" type="submit" value="submit" runat="server" onserverclick="LoginClick"/><br />
            <br />
        </div>
    </form>

    <script runat="server">
        public void LoginClick(Object o, EventArgs e)
        {
            if (Password1.Value == Password2.Value)
            {
                Response.Write(String.Format("Thanks {0} for login", Text1.Value));
            }
        }
    </script>
</body>
</html>
