﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForm
{
    public partial class Exercise55SlideAspNetIntro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (TextBox4.Text != "" && TextBox3.Text != "" && TextBox1.Text != "")
            {
                if (TextBox4.Text == TextBox3.Text)
                {
                    Server.Transfer("~/Exercise55Result.aspx");
                }
            }

            else
            {
                //Error is hidden by default
                Label2.Visible = true;
                Label2.Text = "The data you input is invalid";
            }
            
        }
    }
}