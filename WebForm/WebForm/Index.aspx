﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WebForm.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <br />
            <input type="text" runat="server" id="txtName" />
            <input type="submit" runat="server" onserverclick="SayHello"/>
        </div>
    </form>

    <script runat="server">
        public void SayHello(Object o, EventArgs e)
        {
            Response.Write("Hello " + txtName.Value);
        }
    </script>
</body>
</html>
